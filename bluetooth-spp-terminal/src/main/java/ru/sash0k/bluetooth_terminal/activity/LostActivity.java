package ru.sash0k.bluetooth_terminal.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import ru.sash0k.bluetooth_terminal.R;

public class LostActivity extends Activity {

    MediaPlayer bluetoothDisconnected;
    MediaPlayer success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lost);

        bluetoothDisconnected = MediaPlayer.create(this, R.raw.coocooclocksound);
        success = MediaPlayer.create(this,R.raw.tada);

        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter1);
        this.registerReceiver(mReceiver, filter2);
        this.registerReceiver(mReceiver, filter3);

        bluetoothDisconnected.start();
        bluetoothDisconnected.setLooping(true);

    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            //Context context = getApplicationContext();
            //CharSequence text = "Hello toast!";
            int duration = Toast.LENGTH_SHORT;



            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //Device found
                Toast.makeText(context, "Device found", duration).show();
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Device is now connected
                Toast.makeText(context, "Device is now connected!", duration).show();
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Done searching
                Toast.makeText(context, "Done searching", duration).show();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                //Device is about to disconnect
                Toast.makeText(context, "Device is about to disconnect", duration).show();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Device has disconnected
                Toast.makeText(context, "Device has disconnected!", duration).show();
                //Snackbar.make()

                // Play sound
                bluetoothDisconnected.start();
            }
        }
    };

    int flag = 0;

    public void AlarmOffButton(View v) {
        if (flag == 0) {
            bluetoothDisconnected.stop();
            bluetoothDisconnected.release();
        }
        flag = 1;
    }


    public void foundItButton(View v) {

        if (flag == 0){
            // alarm off
            bluetoothDisconnected.stop();
            bluetoothDisconnected.release();
        }

        success.start();

        // Open MainActivity
        Intent intent = new Intent(this, DeviceControlActivity.class);
        startActivity(intent);

    }
}
